import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Routes, Route} from "react-router-dom";

import Links from "./Components/Links";
import Phase1 from "./Components/Phase1"
import Phase2 from "./Components/Phase2"
import Phase3 from "./Components/Phase3"
import Phase4 from "./Components/Phase4"
import Phase5 from "./Components/Phase5"
import Footer from "./Components/Footer"
import Header from "./Components/Header"

function App() {
  return (
    <BrowserRouter>
        <div className="App">
          <Header/>
          <Links/>

          <Routes>
            <Route path="/" element={<Phase1/>}/>
            <Route path="/phase2" element={<Phase2/>}/>
            <Route path="/phase3" element={<Phase3/>}/>
            <Route path="/phase4" element={<Phase4/>}/>
            <Route path="/phase5" element={<Phase5/>}/>
          </Routes>

          <Footer/>
        </div>
    </BrowserRouter>
  );
}

export default App;
