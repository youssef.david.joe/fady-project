import React, { useState, useRef } from 'react';
import { Container , Row , Col ,Form , Button } from "react-bootstrap";

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
  } from "chart.js";
import { Line } from "react-chartjs-2";


ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
  );

const Phase2=()=>{
  const R = useRef(null);
  const V = useRef(null);
  const F = useRef(null);
  const [result, setResult] = useState(null);
  const handleSubmit=(event) => {
      event.preventDefault();
      const revenue = R.current.value;
      const variable = V.current.value;
      const fixed = F.current.value;
      const result = fixed/(revenue-variable);
      setResult(result);
      return false;
  }
return(
  <div className="phase1 pt-5 pb-5">
      <Container>
          <Row>
              <Col lg={12}>
                  <h1 className="text-center">Break Even Point Calculation</h1>
              </Col>
              <Col lg={6} className="mt-5">
                  <Form>
                      <Form.Group>
                          <Form.Label>Enter Revenue</Form.Label>
                          <Form.Control type="number" ref={R} />
                      </Form.Group>
                      <Form.Group className="mb-3">
                          <Form.Label>Enter Variable Cost</Form.Label>
                          <Form.Control type="number" ref={V} />
                      </Form.Group>
                      <Form.Group className="mb-3">
                          <Form.Label>Enter Fixed Cost</Form.Label>
                          <Form.Control type="number" ref={F} />
                      </Form.Group>
                      <Button variant="primary" type="submit" className="justify-content-center" onClick={handleSubmit} >
                          Calculate and Draw
                      </Button>
                  </Form>

              </Col>
              <Col lg={6} className="pt-5">
              { result ? <h5 className="pt-3 mb-4">Number of unit: {result}</h5> : '' }
{
(R.current?.value && V.current?.value && F.current?.value && result) ? (
              <Line
              datasetIdKey='id'
              data={{
                labels: [0, result],
                datasets: [
                  {
                    id: 1,
                    label: 'Revenue',
                    data: [0, (result * R.current?.value)],
                  },
                  {
                      id: 2,
                      label: 'Cost',
                      data: [F.current?.value, (result * V.current?.value)+ Number(F.current?.value)],
                    }
                ],
              }}
              />
): '' }
              </Col>
          </Row>
      </Container>

  </div>
)
}

export default Phase2;
