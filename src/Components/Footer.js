import React from "react";
import { Container , Row, Col} from "react-bootstrap";
import './Footer.css'
import DrImage from './dr.jpeg'


const Footer =()=>{
    return(
        <div className="footer pt-5 pb-5">
            <Container>
                <Row>
                    <Col lg={3} className='dr-card'>
                        <h3 className="mb-5">Team Supervisor</h3>
                        <img src={DrImage} alt='Dr.Ibrahim Sabry'/>
                        <h5>Dr.Ibrahim Sabry</h5>
                    </Col>
                    <Col lg={3}></Col>
                    <Col lg={6}>
                        <h3 className="mb-5">Team Members</h3>
                        <Row>
                            <Col lg={6}>
                                <p>Abdallah Salah Mohamed Ahmed</p>
                                <p>Abdallah Abdelsalam Musa</p>
                                <p>Abdelmaged Elsayed Abdelmaged</p>
                                <p>Ali Bassiouni Ali Al-Hilli</p>
                                <p>Omar Sharaf Ali Sharaf</p>
                                <p>Mahmoud Essam Antar Ibrahim</p>
                                <p>Mahmoud Abdelaziz Fouad Mohamed</p>
                            </Col>
                            <Col lg={6}>
                                <p>Fady Daood Youssef</p>
                                <p>Muhannad Yahya Zakaria Abdel Halim Al-Qadi</p>
                                <p>Muhammad Munir Rahman Ibrahim</p>
                                <p>Mohamed Abdel Halim El-Sayed Mohamed</p>
                                <p>Mohammadi Khaled Mohammadi</p>
                                <p>Mostafa Yousry Mohamed Hashala</p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Footer;