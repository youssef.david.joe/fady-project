import React, { useState, useRef } from 'react';
import { Container , Row , Col ,Form , Button } from "react-bootstrap";

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
  } from "chart.js";
import { Line } from "react-chartjs-2";


ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
  );

const Phase4=()=>{
  const R = useRef(null);
  const V = useRef(null);
  const U = useRef(null);
  const F = useRef(null);
  const [B, setB] = useState(null);
  const handleSubmit=(event) => {
      event.preventDefault();
      const revenue = Number(R.current.value);
      const unit = Number(U.current.value);
      const fixed = Number(F.current.value);
      const result = ((revenue * unit) - fixed)/unit;
      setB(result);
      return false;
  }
return(
  <div className="phase1 pt-5 pb-5">
      <Container>
          <Row>
              <Col lg={12}>
                  <h1 className="text-center">Variable Cost Per Unit Calculation</h1>
              </Col>
              <Col lg={6} className="mt-5">
                  <Form>
                      <Form.Group>
                          <Form.Label>Enter Revenue</Form.Label>
                          <Form.Control type="number" ref={R} />
                      </Form.Group>
                      <Form.Group className="mb-3">
                          <Form.Label>Enter number of unit</Form.Label>
                          <Form.Control type="number" ref={U} />
                      </Form.Group>
                      <Form.Group className="mb-3">
                          <Form.Label>Enter Fixed Cost</Form.Label>
                          <Form.Control type="number" ref={F} />
                      </Form.Group>
                      <Button variant="primary" type="submit" className="justify-content-center" onClick={handleSubmit} >
                          Calculate and Draw
                      </Button>
                  </Form>

              </Col>
              <Col lg={6} className="pt-5">
                { B ? <h5 className="pt-3 mb-4">Variable Cost: {B}</h5> : '' }
              {
              (R.current?.value && B && U.current?.value && F.current?.value) ? (
              <Line
              datasetIdKey='id'
              data={{
                labels: [0, U.current?.value],
                datasets: [
                  {
                    id: 1,
                    label: 'Revenue',
                    data: [0, (U.current?.value * R.current?.value)],
                  },
                  {
                      id: 2,
                      label: 'Cost',
                      data: [F.current?.value, (U.current?.value * B)+ Number(F.current?.value)],
                    }
                ],
              }}
              />
              ): '' }
              </Col>
          </Row>
      </Container>

  </div>
)
}

export default Phase4;
