import React from "react";
import { Container , Row, Col} from "react-bootstrap";

import './Header.css'
import Logo from './logo.png';
import LogoBanha from './logo_banha.png';

const Header=()=>{
    return(
        <div className="header">
            <Container className="pt-3 pb-3 mb-5">
                <Row>
                    <Col lg={3} className="pt-3">
                        <img src={Logo} alt="Logo"/>
                    </Col>
                    <Col lg={7} className="text-center">
                        <h2>Banha University</h2>
                        <h2>Faculty of Engineering</h2>
                    </Col>
                    <Col lg={2}>
                        <img src={LogoBanha} alt="Logo" className="logo_banha"/>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Header