import React from "react";


import { Link } from "react-router-dom";

import './Links.css';


const Links=()=>{
  return(

    <div className="Links">
        <ul className="list-unstyled d-flex justify-content-center">
            <li className="p-3 m-3"><Link to="/">Benefit</Link></li>
            <li className="p-3 m-3"><Link to="/phase2">Break Even Number</Link></li>
            <li className="p-3 m-3"><Link to="/phase3">Revenue</Link></li>
            <li className="p-3 m-3"><Link to="/phase4">Variable Cost</Link></li>
            <li className="p-3 m-3"><Link to="/phase5">Number of units</Link></li>
        </ul>

    </div>

  )
}

export default Links;