import React, { useState, useRef } from 'react';
import { Container , Row , Col ,Form , Button } from "react-bootstrap";

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
  } from "chart.js";
import { Line } from "react-chartjs-2";


ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
  );

const Phase3=()=>{
  const R = useRef(null);
  const V = useRef(null);
  const U = useRef(null);
  const F = useRef(null);
  const P = useRef(null);
  const [result, setResult] = useState(null);
  const handleSubmit=(event) => {
      event.preventDefault();
      const variable = Number(V.current.value);
      const unit = Number(U.current.value);
      const fixed = Number(F.current.value);
      const percentage = Number(P.current.value);
      const revenue = ((fixed+(unit*variable))/unit) * (1+(percentage/100));
      setResult(revenue);
      return false;
  }
return(
  <div className="phase1 pt-5 pb-5">
      <Container>
          <Row>
              <Col lg={12}>
                  <h1 className="text-center">Revenue Per Unit Calculation</h1>
              </Col>
              <Col lg={6} className="mt-5">
                  <Form>
                      <Form.Group className="mb-3">
                          <Form.Label>Enter Variable Cost</Form.Label>
                          <Form.Control type="number" ref={V} />
                      </Form.Group>
                      <Form.Group className="mb-3">
                          <Form.Label>Enter number of unit</Form.Label>
                          <Form.Control type="number" ref={U} />
                      </Form.Group>
                      <Form.Group className="mb-3">
                          <Form.Label>Enter Fixed Cost</Form.Label>
                          <Form.Control type="number" ref={F} />
                      </Form.Group>
                      <Form.Group className="mb-3">
                          <Form.Label>Enter Percentage of revenue %</Form.Label>
                          <Form.Control type="number" ref={P} />
                      </Form.Group>
                      <Button variant="primary" type="submit" className="justify-content-center" onClick={handleSubmit} >
                          Calculate and Draw
                      </Button>
                  </Form>

              </Col>
              <Col lg={6} className="pt-5">
              { result ? <h5 className="pt-3 mb-4">Revenue: {result}</h5> : '' }
{
(result && V.current?.value && U.current?.value && F.current?.value) ? (
              <Line
              datasetIdKey='id'
              data={{
                labels: [0, U.current?.value],
                datasets: [
                  {
                    id: 1,
                    label: 'Revenue',
                    data: [0, (U.current?.value * result) * (1+(P.current?.value/100))],
                  },
                  {
                      id: 2,
                      label: 'Cost',
                      data: [F.current?.value, (U.current?.value * V.current?.value)+ Number(F.current?.value)],
                    }
                ],
              }}
              />
): '' }
              </Col>
          </Row>
      </Container>

  </div>
)
}

export default Phase3;
